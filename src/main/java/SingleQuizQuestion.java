import java.util.List;

public class SingleQuizQuestion {
    private String question;

    public SingleQuizQuestion(String question, String correctAnswer, List<String> badAnswers) {
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.badAnswers = badAnswers;
    }

    private String correctAnswer;
    private List<String> badAnswers;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public List<String> getBadAnswers() {
        return badAnswers;
    }

    public void setBadAnswers(List<String> badAnswers) {
        this.badAnswers = badAnswers;
    }
}
