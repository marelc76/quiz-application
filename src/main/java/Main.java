import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("src/main/resources/quiz.txt");
        readAllQuizQuestionsFromFile(file);
    }

    public static List<SingleQuizQuestion> readAllQuizQuestionsFromFile(File file) throws FileNotFoundException {
        Scanner sc = new Scanner(file);
        List<SingleQuizQuestion> quizQuestions = new ArrayList<SingleQuizQuestion>();
        while (sc.hasNextLine()) {
            String pytanie = sc.nextLine();
            String dobraOdp = sc.nextLine();
            ArrayList<String> listaZlychPytan = new ArrayList<String>(3);
            for (int i = 0; i < 3; i++) {
                listaZlychPytan.add(sc.nextLine());
            }
            SingleQuizQuestion singleQuizQuestion = new SingleQuizQuestion(pytanie, dobraOdp, listaZlychPytan);
            quizQuestions.add(singleQuizQuestion);
        }
        System.out.println("");
        return quizQuestions;

    }


}
